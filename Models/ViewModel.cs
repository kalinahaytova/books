﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace BookManager.Models
{
    public class ViewModel
    {
        public int ID { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public ViewModel()
        { }

        public ViewModel(ViewModel model)
        {
            ID = model.ID;
            CreatedAt = model.CreatedAt;
            UpdatedAt = model.UpdatedAt;
        }
    }
}