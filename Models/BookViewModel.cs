﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookManager.Models
{
    public class BookViewModel
    {
        public string Title { get; set; }

        public int AuthorID { get; set; }
        public  AuthorViewModel Author { get; set; }

        public int GenreID { get; set; }
        public  GenreViewModel Genre { get; set; }

        
    }
}