﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BookManager.Models;

namespace BookManager.Controllers
{
    public class BooksController : Controller
    {
        private UnitOfWork _uow;

        public BooksController()
        {
            _uow = new UnitOfWork();
        }
        // GET: Books
        public ActionResult Index(string queryString)
        {
            List<Book> books;
            if (queryString != null && queryString != "")
            {
                books = _uow.BookRepository.SearchByString(queryString);
            }
            else
            {
                books = _uow.BookRepository.GetAll();
            }
            List<BookViewModel> bookViewModels = BookViewModel.ToList(books);
            return View(bookViewModels);
        }

        // GET: Books/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = _uow.BookRepository.GetByID((int)id);
            if (book == null)
            {
                return HttpNotFound();
            }
            BookViewModel bookViewModel = new BookViewModel(book);
            return View(BookViewModel);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            MakeAuthorsViewBag();
            MakeGenreViewBag();

            return View();
        }

        // POST: Books/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Books/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Books/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Books/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
